import click
import json

db_file = None
master_key = None

@click.group()
@click.option('--db', help="path to jsdb file")
def cli(db):
    if db is not None:
        global db_file
        db_file = db
        global master_key
        master_key = db.split('/')[-1].split('.')[0] # TODO: use the path library

@cli.command()
@click.option('--key', prompt='Top level object name', help='key for top level object')
def init(key):
    print "To exit, input 'jsondb'"
    top_level = []
    while True:
        name = raw_input("{}: ".format(key))
        if name == "jsondb":
            break
        top_level.append({key: name})
    
    tl_str = json.dumps(top_level)

    with open("{}.jsdb".format(key), 'w') as f:
        f.write(tl_str)

@cli.command()
@click.option('--key', prompt="New key", help='Key to add')
def add_property(key):
    data = load_data()

    for obj in data:
        value = raw_input(obj[master_key] + ": ")
        obj[key] = value

    write_data(data)

@cli.command()
@click.option('--old', prompt=True, help='old key name')
@click.option('--new', prompt=True, help="new key name")
def rename_key(old, new):
    data = load_data()

    for obj in data:
        value = obj.pop(old, None)
        obj[new] = value

    write_data(data)


def load_data():
    data = None
    with open(db_file, 'r') as f:
        data = json.loads(f.read())
    return data


def write_data(data):
    with open(db_file, 'w') as f:
        f.write(json.dumps(data))

if __name__ == "__main__":
    cli()
